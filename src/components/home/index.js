import React, { Component, useState } from 'react';
import withLayout from './../layout';
import axios from 'axios';

const STATE = [{ key: 'bjohor', name: 'Johor' }, { key: 'bpenang', name: 'Penang' }, { key: 'bsarawak', name: 'Sarawak' }, { key: 'bselangor', name: 'Selangor' }, { key: 'bkl', name: 'W.P Kuala Lumpur' }]

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentState: 'bjohor',
      total: 0,
    }
  }

  fetchData = () => {
    axios.get("https://api.vaksincovid.gov.my/az/?action=listppv")
    .then((response) => {
      // console.log("response")
      // console.log("total remaining ===> ", response.data.data.map((d) => ( parseInt(d.balance) )).reduce((a,b) => ( a + b )))
      this.setState({
        total: response.data.data.map((d) => ( parseInt(d.balance) )).reduce((a,b) => ( a + b )) || 0
      })
    })  
  };

  onChangeState = (value) => {
    this.setState({
      currentState: value
    })
  }

  numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  componentDidMount() {
    setInterval(() => {
      this.fetchData();
    }, 10)
  }

  render() {
    
    return (
      <div style={{ height: '100%', paddingTop: '8%', paddingHorizontal: '5%', position: 'relative', textAlign: 'center' }}>
        <h1 style={{ fontWeight: 'bold' }}>Malaysia AstraZeneca Vaccine Registration Slot</h1>
        <select default={this.state.currentState} name="State" id="state" onChange={(event) => { this.onChangeState(event.target.value) }}>
        {
          STATE.map((state) => {
            return (
                <option value={state.key}>{state.name}</option>
              )
          })
        }
        </select>
        <h1 style={{ fontSize: '12vw', fontWeight: 'bold', color: '#3CB64C' }}>{this.numberWithCommas(this.state.total || 0)}</h1>
        <h3>Visit <a target="_blank" href="https://www.vaksincovid.gov.my/"><b>vaksincovid.gov.my</b></a> for more information.</h3>
        <h3>Data fetched by <a target="_blank" href="https://zarulrahim.com"><b>Zarul Rahim</b></a></h3>
      </div>
    );
  }
}

export default withLayout(Home);
